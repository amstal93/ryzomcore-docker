# Ryzom Core Docker

## Motivation

Docker greatly simplifies the development workflow in that it allows
for a local shard to be spun up with a single command, 
lowering the barrier of entry for new contributors.

[Attempts](https://github.com/ryzom/ryzomcore/issues/291) have been made to
upstream these sources, but until then these images have been made available
under the HexGear Studio organization on Docker Hub.

## Acknowledgement

Based on [Ryzom Core](https://github.com/ryzom), the open-source project behind
[Ryzom](https://ryzom.com), a unique science fantasy MMORPG.

## License

These sources are licensed under the [GNU General Public License 3.0](COPYING) or any later version.
