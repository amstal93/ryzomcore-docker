# GitLab CI/CD

The timeout setting for the pipeline should be set to two hours so that
`docker-compose` has time to compile all of the images:

```yml
Timeout: 2h
```

## Reference

- [GitLab Settings Pipeline Timeout](https://gitlab.com/help/ci/pipelines/settings#timeout)
